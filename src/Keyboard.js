import React from 'react'
import './Keyboard.css'

const Keyboard = ({ keys, usedLetters, victory, onClick, restartPendu }) => (
  <div className="keyboard">
  
    {!victory && keys.map(letter => (
      <span
        key={letter}
        victory={victory}
        className={usedLetters.has(letter) ? 'key used' : 'key'}
        onClick={() => onClick(letter)}
      >
        {letter.toUpperCase()}
      </span>
    ))}
    {victory 
      && <button onClick={() => restartPendu("init")}>Recommencer</button>}
  </div>
)


export default Keyboard
