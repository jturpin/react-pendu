import React, { Component } from 'react'

import './App.css'

import Keyboard from './Keyboard'

var wd = require("word-definition");
var frenchDic = require("an-array-of-french-words")
//Tous les mots du dictionnaire qui commencent par "a"
const phrases = frenchDic.filter(word => !!word.match(/^fun/i))
//const phrases = frenchDic

class App extends Component {
  constructor() {
    super()
    this.state = {
      keys: [
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z',
      ],
      phrase: this.phraseRandom(phrases),
      usedLetters: new Set(),
      score: 0,
      victory: 0,
    }
  }



  phraseRandom(phrases) {
    const min = Math.ceil(0)
    const max = Math.floor(phrases.length - 1)
    const nbRandom = Math.floor(Math.random() * (max - min + 1)) + min
    //on retire les accents des mots trouvé dans le dico
    return phrases[nbRandom].normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    //return phrases[nbRandom]
  }

  // Produit une représentation textuelle de l’état de la partie,
  // chaque lettre non découverte étant représentée par un _underscore_.
  // (CSS assurera de l’espacement entre les lettres pour mieux
  // visualiser le tout).
  computeDisplay(phrase, usedLetters) {
    return phrase.replace(
      /\w/g,
      letter => (usedLetters.has(letter) ? letter : '_')
    )
  }

  handleKeyClick = letter => {
    // Gestion des lettres utilisées et incrément du score
    const { usedLetters } = this.state
    usedLetters.add(letter)
    this.setState({ usedLetters, score: this.state.score + 1 })

    // Message de Victoire
    const myPhrase = new Set(this.state.phrase)
    if (new Set([...myPhrase].filter(x => !usedLetters.has(x))).size === 0) {
      this.setState({victory: 1})
      alert('Victoire en ' + this.state.score + ' tentatives')
    }
  }

  restartPendu = param => {
    //Réinitialisation de la partie
    const newPhrase = this.phraseRandom(phrases)
    this.setState({ phrase: newPhrase, usedLetters: new Set(), score: 0, victory:0 })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Le Pendu</h1>
        </header>
        <div className="App-screen">
          <p className="App-score">
            Nombre de tentative(s) : {this.state.score}
          </p>
          <p className="App-wordSearch">
            {this.computeDisplay(this.state.phrase, this.state.usedLetters)}
          </p>
          {
            this.state.victory &&  
            <span className="definition">{wd.getDef(this.state.phrase, "fr", null , function(definition) {
              console.log(definition)
          })}</span>
          }
        </div>
        <Keyboard
          keys={this.state.keys}
          victory={this.state.victory}
          usedLetters={this.state.usedLetters}
          onClick={this.handleKeyClick}
          restartPendu={this.restartPendu}
        />
      </div>
    )
  }
}

export default App
